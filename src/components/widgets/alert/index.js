import * as React from "react";
import Alert from "@mui/material/Alert";
import alertContext from "../../../context/alert/alertContext";

export default function BasicAlerts(props) {
  const { alert } = React.useContext(alertContext);

  return (
    <>
      <div>
        {alert.message && (
          <Alert severity={alert.severity}>{alert.message}</Alert>
        )}
      </div>
    </>
  );
}
