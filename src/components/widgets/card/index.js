import React, { useContext } from "react";
import noteContext from "../../../context/notes/noteContext";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import ModeEditOutlineIcon from "@mui/icons-material/ModeEditOutline";
import alertContext from "../../../context/alert/alertContext";

export default function ActionAreaCard(props) {
  let notesContext = useContext(noteContext);
  let { deleteNote } = notesContext;
  const { showAlert } = useContext(alertContext);

  return (
    <Card sx={{ maxWidth: 345, m: 2 }}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {props.note.title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {props.note.description}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {props.note.tag}
          </Typography>

          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
            }}
          >
            <Box
              sx={{
                mt: 1,
              }}
            >
              <div
                onClick={() => {
                  deleteNote(props.note._id);
                  showAlert("success", "Deleted successfully");
                }}
              >
                <DeleteIcon />
              </div>
            </Box>
            <Box
              sx={{
                mt: 1,
                ml: 1,
              }}
            >
              <div onClick={props.handleOpen}>
                {/* //?? we can use two Function in same div/button */}
                <div
                  onClick={() => {
                    console.log(props.note);
                    props.updateNote(props.note);
                  }}
                >
                  <ModeEditOutlineIcon />
                </div>
              </div>
            </Box>
          </Box>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
