import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { Link, useLocation } from "react-router-dom";
import Container from "@mui/material/Container";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { useNavigate } from "react-router-dom";

const darkTheme = createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "#1976d2",
    },
  },
});
// let [disable, setDisable] = React.useState(true);
let Navbar = function () {
  let location = useLocation();
  let navigate = useNavigate();

  // React.useEffect(() => {
  //   console.log(location.pathname);
  // }, [location]);
  let handelLogout = function () {
    localStorage.removeItem("token");
    navigate("/login");
  };

  return (
    <>
      <ThemeProvider theme={darkTheme}>
        <AppBar position="static">
          <Container maxWidth="xl">
            <Toolbar disableGutters>
              <Typography
                variant="h6"
                noWrap
                component="div"
                sx={{ mr: 2, display: { xs: "none", md: "flex" } }}
              >
                iNotebook
              </Typography>
              <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
                <Button
                  disabled={!localStorage.getItem("token")}
                  component={Link}
                  to="/"
                  variant={`${
                    location.pathname === "/" ? "outlined" : "contained"
                  }`}
                  color="primary"
                  sx={{ color: "white" }}
                >
                  Home
                </Button>
                <Button
                  disabled={!localStorage.getItem("token")}
                  component={Link}
                  to="/about"
                  variant={`${
                    location.pathname === "/about" ? "outlined" : "contained"
                  }`}
                  color="primary"
                >
                  About
                </Button>
              </Box>

              {!localStorage.getItem("token") ? (
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    p: 1,
                    m: 1,
                  }}
                >
                  <Button
                    component={Link}
                    to="/login"
                    variant={`${
                      location.pathname === "/login" ? "outlined" : "contained"
                    }`}
                    color="primary"
                  >
                    Login
                  </Button>
                  <Button
                    component={Link}
                    to="/signUp"
                    variant={`${
                      location.pathname === "/signUp" ? "outlined" : "contained"
                    }`}
                    color="primary"
                  >
                    SignUp
                  </Button>
                </Box>
              ) : (
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    p: 1,
                    m: 1,
                  }}
                >
                  <Button
                    onClick={handelLogout}
                    component={Link}
                    to="/login"
                    variant={`${
                      location.pathname === "/login" ? "outlined" : "contained"
                    }`}
                    color="primary"
                  >
                    Log out
                  </Button>
                </Box>
              )}
            </Toolbar>
          </Container>
        </AppBar>
      </ThemeProvider>
    </>
  );
};

export default Navbar;
