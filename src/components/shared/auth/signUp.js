import React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import axios from "../../../axios";
import { useNavigate } from "react-router-dom";
import alertContext from "../../../context/alert/alertContext";

const SignUp = () => {
  const { showAlert } = React.useContext(alertContext);

  let [credentials, setCredentials] = React.useState({
    name: "",
    email: "",
    password: "",
    cpassword: "",
  });
  let navigate = useNavigate();
  let handelClick = (e) => {
    e.preventDefault();
    const { name, email, password } = credentials;
    axios
      .post(`http://localhost:5000/api/auth/createuser`, {
        name,
        email,
        password,
      })
      .then((res) => {
        console.log(res.data.authToken);
        console.log(res.data);
        showAlert("success", "Account Created Successfully");
        localStorage.setItem("token", res.data.authToken);
        navigate("/");
      })
      .catch((e) => {
        showAlert("error", e.response.data.errors);
        console.log(e.response.data);
      });
  };
  let onChange = (e) => {
    setCredentials({ ...credentials, [e.target.name]: e.target.value });
  };
  return (
    <div>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          p: 1,
          m: 1,
          bgcolor: "background.paper",
        }}
      >
        <Box>
          <h2>SignUp</h2>

          <Box
            sx={{
              p: 1,
              m: 1,
              bgcolor: "background.paper",
            }}
          >
            <TextField
              name="name"
              id="name"
              label="Name"
              variant="outlined"
              value={credentials.name}
              onChange={onChange}
            />
          </Box>
          <Box
            sx={{
              p: 1,
              m: 1,
              bgcolor: "background.paper",
            }}
          >
            <TextField
              name="email"
              id="email"
              label="Email"
              variant="outlined"
              value={credentials.email}
              onChange={onChange}
            />
          </Box>
          <Box
            sx={{
              p: 1,
              m: 1,
              bgcolor: "background.paper",
            }}
          >
            <TextField
              name="password"
              id="password"
              label="Password"
              variant="outlined"
              type="password"
              value={credentials.password}
              onChange={onChange}
            />
          </Box>
          <Box
            sx={{
              p: 1,
              m: 1,
              bgcolor: "background.paper",
            }}
          >
            <TextField
              name="cpassword"
              id="cpassword"
              label="Confirm Password"
              variant="outlined"
              type="password"
              value={credentials.cpassword}
              onChange={onChange}
            />
          </Box>
          <Button variant="contained" onClick={handelClick}>
            SignUp
          </Button>
        </Box>
      </Box>
    </div>
  );
};

export default SignUp;
