import React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import axios from "../../../axios";
import { useNavigate } from "react-router-dom";
import alertContext from "../../../context/alert/alertContext";

const Login = () => {
  const { showAlert } = React.useContext(alertContext);

  let [credentials, setCredentials] = React.useState({
    email: "",
    password: "",
  });
  let navigate = useNavigate();
  let handelClick = (e) => {
    e.preventDefault();
    axios
      .post(`http://localhost:5000/api/auth/login`, {
        email: credentials.email,
        password: credentials.password,
      })
      .then((res) => {
        showAlert("success", "Logged is  Successfully");
        localStorage.setItem("token", res.data.authToken);
        navigate("/");
      })
      .catch((e) => {
        showAlert("error", e.response.data.errors);
        console.log(e.response.data);
      });
  };
  let onChange = (e) => {
    setCredentials({ ...credentials, [e.target.name]: e.target.value });
  };
  return (
    <div>
      <h2>Login</h2>
      <Box
        sx={{
          p: 1,
          m: 1,
          bgcolor: "background.paper",
        }}
      >
        <TextField
          name="email"
          id="email"
          label="Email"
          variant="outlined"
          value={credentials.email}
          onChange={onChange}
        />
      </Box>
      <Box
        sx={{
          p: 1,
          m: 1,
          bgcolor: "background.paper",
        }}
      >
        <TextField
          name="password"
          id="password"
          label="Password"
          variant="outlined"
          type="password"
          value={credentials.password}
          onChange={onChange}
        />
      </Box>
      <Button variant="contained" onClick={handelClick}>
        Login
      </Button>
    </div>
  );
};

export default Login;
