import React, { useContext } from "react";
import noteContext from "../../context/notes/noteContext";
import Card from "../widgets/card";
import Box from "@mui/material/Box";
import AddNote from "./add-note";
import Grid from "@mui/material/Grid";
import Modal from ".././widgets/modal";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import alertContext from "../../context/alert/alertContext";

let NoteItem = function () {
  let notesContext = useContext(noteContext);
  let navigate = useNavigate();
  let { notes, getNote, editNote } = notesContext;
  const { showAlert } = useContext(alertContext);

  React.useEffect(() => {
    getNote();
  }, [notes]);

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    console.log(note);
    setOpen(true);
  };
  let [note, setNote] = React.useState({
    _id: "",
    etitle: "",
    edescription: "",
    etag: "",
  });
  let handelClick = (e) => {
    debugger;
    setOpen(false);
    editNote(note._id, note.etitle, note.edescription, note.etag);
    showAlert("success", "Updated successfully");

    console.log("Updating The Note ", note);
  };
  let onChange = (e) => {
    setNote({ ...note, [e.target.name]: e.target.value });
  };

  const updateNote = (currentNote) => {
    setNote({
      _id: currentNote._id,
      etitle: currentNote.title,
      edescription: currentNote.description,
      etag: currentNote.tag,
    });
    console.log("run");
  };
  localStorage.getItem("token");
  return (
    <>
      <AddNote />
      <Modal open={open}>
        <h2>Update Note</h2>
        <Box
          sx={{
            p: 1,
            m: 1,
            bgcolor: "background.paper",
          }}
        >
          <TextField
            name="etitle"
            id="etitle"
            label="Title"
            variant="outlined"
            value={note.etitle}
            onChange={onChange}
          />
        </Box>
        <Box
          sx={{
            p: 1,
            m: 1,
            bgcolor: "background.paper",
          }}
        >
          <TextField
            name="edescription"
            id="edescription"
            label="Description"
            variant="outlined"
            value={note.edescription}
            onChange={onChange}
          />
        </Box>
        <Box
          sx={{
            p: 1,
            m: 1,
            bgcolor: "background.paper",
          }}
        >
          <TextField
            name="etag"
            id="etag"
            label="Tag"
            variant="outlined"
            value={note.etag}
            onChange={onChange}
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row-reverse",
            p: 1,
            m: 1,
          }}
        >
          <Button
            disabled={note.etitle.length <= 5 && note.edescription.length <= 5}
            onClick={handelClick}
            variant="contained"
          >
            Update Note
          </Button>
        </Box>
      </Modal>
      <h2>You Notes</h2>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          p: 1,
          m: 1,
          bgcolor: "background.paper",
        }}
      >
        <Grid container justifyContent="  center" Spacing={3}>
          {notes.length === 0 && "No Notes To Display"}

          {notes.map((note, index) => {
            return (
              <React.Fragment key={index}>
                <Grid item lg={4} md={6} sm={6}>
                  <Card
                    key={note._id}
                    updateNote={updateNote}
                    handleOpen={handleOpen}
                    note={note}
                  />
                </Grid>
              </React.Fragment>
            );
          })}
        </Grid>
      </Box>
    </>
  );
};
export default NoteItem;
