import React, { useContext } from "react";
import noteContext from "../../context/notes/noteContext";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import alertContext from "../../context/alert/alertContext";

const AddNote = () => {
  let notesContext = useContext(noteContext);
  let { addNote } = notesContext;
  const { showAlert } = useContext(alertContext);

  let [note, setNote] = React.useState({
    title: "",
    description: "",
    tag: "",
  });

  let handelClick = (e) => {
    e.preventDefault();
    addNote(note.title, note.description, note.tag);
    setNote({ title: "", description: "", tag: "" });
    showAlert("success", "Added note successfully");
  };
  let onChange = (e) => {
    setNote({ ...note, [e.target.name]: e.target.value });
  };
  return (
    <div>
      <h2>Add a Note</h2>
      <Box
        sx={{
          p: 1,
          m: 1,
          bgcolor: "background.paper",
        }}
      >
        <TextField
          name="title"
          id="title"
          label="Title"
          variant="outlined"
          value={note.title}
          onChange={onChange}
        />
      </Box>
      <Box
        sx={{
          p: 1,
          m: 1,
          bgcolor: "background.paper",
        }}
      >
        <TextField
          name="description"
          id="description"
          label="Description"
          variant="outlined"
          value={note.description}
          onChange={onChange}
        />
      </Box>
      <Box
        sx={{
          p: 1,
          m: 1,
          bgcolor: "background.paper",
        }}
      >
        <TextField
          name="tag"
          id="tag"
          label="Tag"
          variant="outlined"
          value={note.tag}
          onChange={onChange}
        />
      </Box>
      <Button
        disabled={note.title.length <= 5 && note.description.length <= 5}
        variant="contained"
        onClick={handelClick}
      >
        Add note
      </Button>
    </div>
  );
};

export default AddNote;
