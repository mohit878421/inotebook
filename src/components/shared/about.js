import React, { useContext } from "react";
import notecontext from "../../context/notes/noteContext";

let About = function () {
  const a = useContext(notecontext);
  return <>About {a.name}
  </>;
};
export default About;
