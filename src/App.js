import React, { Fragment, useContext } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./components/widgets/navbar";
import Home from "./components/shared/home";
import About from "./components/shared/about";
import Login from "./components/shared/auth/login";
import SignUp from "./components/shared/auth/signUp";
import NoteState from "./context/notes/NoteState";
import Container from "@mui/material/Container";
import Alert from "./components/widgets/alert";
import AlertState from "./context/alert/alertState";

function App() {
  return (
    <NoteState>
      <AlertState>
        <Router>
          <Fragment>
            <Navbar />
            <Alert />
            <Container>
              <Routes>
                <Route extra path="/" element={<Home />} />
                <Route extra path="/about" element={<About />} />
                <Route extra path="/login" element={<Login />} />
                <Route extra path="/signUp" element={<SignUp />} />
              </Routes>
            </Container>
          </Fragment>
        </Router>
      </AlertState>
    </NoteState>
  );
}

export default App;
