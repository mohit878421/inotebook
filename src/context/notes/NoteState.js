import NoteContext from "./noteContext";
import React from "react";
import axios from "../../axios";
const NoteState = (props) => {
  const host = "http://localhost:5000";
  const notesInitial = [];
  const [notes, setNotes] = React.useState(notesInitial);
  // Fetch all notes
  let getNote = () => {
    axios.get(`${host}/api/notes/fetchallnotes`).then((res) => {
      setNotes(res.data);
      console.log(res.data);
    });
  };

  // Add  notes
  let addNote = (title, description, tag) => {
    axios
      .post(`${host}/api/notes/addnote`, {
        title,
        description,
        tag,
      })
      .then((res) => {
        setNotes(notes.concat(res.data));
      });
  };

  //Delete a Note
  let deleteNote = (id) => {
    // TODO:API call
    axios.delete(`${host}/api/notes/deletenote/${id}`).then((res) => {
      const newNotes = notes.filter((note) => {
        return note._id !== res.data._id;
      });
      setNotes(newNotes);
    });
  };

  //Edit a Note
  let editNote = (id, title, description, tag) => {
    axios
      .put(`${host}/api/notes/updatenote/${id}`, { title, description, tag })
      .then((res) => {
        let notesCopy = [...notes];
        let updatedNote = notesCopy.find((i) => i._id == res.data.note._id);
        updatedNote.description = res.data.note.description;
        updatedNote.tag = res.data.note.tag;
        updatedNote.title = res.data.note.title;
        setNotes(notesCopy);
      });
  };
  return (
    <NoteContext.Provider
      value={{ notes, getNote, addNote, deleteNote, editNote }}
    >
      {props.children}
    </NoteContext.Provider>
  );
};
export default NoteState;
