import AlertContext from "./alertContext";
import React from "react";

let AlertState = (props) => {
  let [alert, setAlert] = React.useState({
    severity: null,
    message: null,
  });
  let showAlert = (severity, message) => {
    debugger;
    setAlert({
      severity: severity,
      message: message,
    });
    setTimeout(() => {
      setAlert({
        severity: null,
        message: null,
      });
    }, 2000);
  };
  return (
    <AlertContext.Provider value={{ alert, showAlert }}>
      {props.children}
    </AlertContext.Provider>
  );
};

export default AlertState;
