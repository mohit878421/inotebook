import axios from "axios";

const instance = axios.create();
instance.interceptors.request.use(function (config) {
  config.headers.common["auth-token"] = localStorage.getItem("token");
  config.headers.common["Content-Type"] = "application/json";
  return config;
});

export default instance;
